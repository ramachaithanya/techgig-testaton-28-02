import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by chaitu on 2/4/18.
 */
public class BaseTestCase {

    private static final int DEFAULT_WAIT_TIME = 15;

    enum Drivers {
        FIREFOX, CHROME
    }

    private static WebDriver firefoxWebDriver;
    private static WebDriver chromeWebDriver;

    public WebDriver getWebDriver(Drivers d) {
        WebDriver driver = null;
        switch (d) {
            case CHROME:
                driver = chromeWebDriver;
                if (driver == null) {
                    System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
                    chromeWebDriver = new ChromeDriver();
                    driver = chromeWebDriver;
                    driver.manage().timeouts().implicitlyWait(DEFAULT_WAIT_TIME, TimeUnit.SECONDS);
                }
                break;
            case FIREFOX:
                driver = firefoxWebDriver;
                if(driver == null) {
                    firefoxWebDriver = new FirefoxDriver();
                    driver = firefoxWebDriver;
                    driver.manage().timeouts().implicitlyWait(DEFAULT_WAIT_TIME, TimeUnit.SECONDS);
                }
        }
        return driver;
    }

    @Before
    public void initDriver() {
        firefoxWebDriver = null;
        chromeWebDriver = null;
    }

    @AfterClass
    public static void tearDown() {
        if (chromeWebDriver != null) {
            chromeWebDriver.quit();
        }
        if (firefoxWebDriver != null) {
            firefoxWebDriver.quit();
        }
    }
}
